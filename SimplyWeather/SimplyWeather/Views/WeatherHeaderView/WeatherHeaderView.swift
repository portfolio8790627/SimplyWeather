//
//  WeatherHeaderView.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.06.2022.
//

import UIKit

class WeatherHeaderView: UIView {
    
    //MARK: Properties
    private let paddingWidth: CGFloat = 20
    private let paddingHeight: CGFloat = 10
    
    private var viewModel: WeatherHeaderViewViewModelProtocol? {
        didSet {
            updateUI()
        }
    }
    
    //MARK: - Views
    private lazy var locationLabel: UILabel = {
        let label = UILabel(
            font: .systemFont(ofSize: 25, weight: .bold),
            color: .white,
            lineLimit: 2,
            alignment: .center
        )
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private lazy var dateLabel = UILabel(color: .white, alignment: .center)
    
    private lazy var currentWeatherView = CurrentWeatherView()
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews([
            locationLabel,
            dateLabel,
            currentWeatherView
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    override func layoutSubviews() {
        calculateSizesOfSubviews()
    }
    
    //MARK: - Methods
    func configure(with viewModel: WeatherHeaderViewViewModelProtocol) {
        self.viewModel = viewModel
    }
    
    //MARK: - Private methods
    private func updateUI() {
        guard let viewModel = viewModel else { return }

        locationLabel.text = "\(viewModel.city)\n\(viewModel.region)"
        dateLabel.text = viewModel.date
        
        currentWeatherView.setup(
            temperature: viewModel.temperature,
            condition: viewModel.condition,
            weatherCode: viewModel.code,
            dayTime: viewModel.dayTime,
            parameters: (
                windSpeed: viewModel.windSpeed,
                humidity: viewModel.humidity,
                visibility: viewModel.visibility
            )
        )
    }
    
    private func calculateSizesOfSubviews() {
        let availableWidth = bounds.width - 2 * paddingWidth
        
        locationLabel.frame = CGRect(
            x: paddingWidth,
            y: paddingHeight,
            width: availableWidth,
            height: 60
        )
        
        dateLabel.frame = CGRect(
            x: paddingWidth,
            y: locationLabel.frame.maxY + 5,
            width: availableWidth,
            height: 20
        )
        
        let availableHeight = bounds.height - (locationLabel.frame.height + paddingHeight) - (dateLabel.frame.height + paddingHeight) - 2 * paddingHeight
        
        currentWeatherView.frame = CGRect(
            x: paddingWidth,
            y: dateLabel.frame.maxY + paddingHeight,
            width: availableWidth,
            height: availableHeight
        )
        
        currentWeatherView.layer.cornerRadius = currentWeatherView.frame.height / 20
    }
}
