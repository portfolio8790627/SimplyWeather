//
//  WeatherParameterIconView.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.06.2022.
//

import UIKit

class WeatherParameterView: UIView {
    
    //MARK: Properties
    private let padding: CGFloat = 5
    
    //MARK: - Views
    private lazy var icon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.tintColor = .white
        return imageView
    }()
    
    private lazy var paramValueLabel = UILabel(
        font: .systemFont(ofSize: 20, weight: .semibold),
        color: .white,
        alignment: .center
    )
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(icon)
        addSubview(paramValueLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    override func layoutSubviews() {
        let numberOfItems: CGFloat = 2
        
        let itemWidth = bounds.width - 2 * padding
        let itemHeight = (bounds.height - padding * (numberOfItems + 1)) / numberOfItems
        
        icon.frame = CGRect(x: padding, y: padding, width: itemWidth, height: itemHeight)
        
        paramValueLabel.frame = CGRect(
            x: padding,
            y: icon.frame.maxY + padding,
            width: itemWidth,
            height: itemHeight
        )
    }
    
    //MARK: - Methods
    func setup(iconImage: UIImage?, valueString: String) {
        icon.image = iconImage
        paramValueLabel.text = valueString
    }
}
