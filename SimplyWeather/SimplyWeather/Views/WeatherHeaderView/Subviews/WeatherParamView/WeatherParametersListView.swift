//
//  WeatherParametersListView.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 01.07.2022.
//

import UIKit

class WeatherParametersListView: UIView {
    
    //MARK: Properties
    private let paddingWidth: CGFloat = 10
    private let paddingHeight: CGFloat = 5
    
    //MARK: - Views
    private lazy var windParamView = WeatherParameterView()
    
    private lazy var humidityParamView = WeatherParameterView()
    
    private lazy var visibilityParamView = WeatherParameterView()
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews([windParamView, humidityParamView, visibilityParamView])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    override func layoutSubviews() {
        let numberOfItems = CGFloat(subviews.count)
        
        let itemWidth = (bounds.width - paddingWidth * (numberOfItems + 1)) / numberOfItems
        let itemHeight = bounds.height - paddingHeight * 2
        
        windParamView.frame = CGRect(
            x: paddingWidth,
            y: paddingHeight,
            width: itemWidth,
            height: itemHeight
        )
        
        humidityParamView.frame = CGRect(
            x: windParamView.frame.maxX + paddingWidth,
            y: paddingHeight,
            width: itemWidth,
            height: itemHeight
        )
        
        visibilityParamView.frame = CGRect(
            x: humidityParamView.frame.maxX + paddingWidth,
            y: paddingHeight,
            width: itemWidth,
            height: itemHeight
        )
    }
    
    //MARK: - Methods
    func setup(windSpeed: Int, humidity: Int, visibility: Int) {
        setupWindParamView(with: windSpeed)
        setupHumidityParamView(with: humidity)
        setupVisibilityParamView(width: visibility)
    }
    
    //MARK: - Private methods
    private func setupWindParamView(with speed: Int) {
        windParamView.setup(iconImage: Resources.Images.Common.wind, valueString: "\(speed)km/h")
    }
    
    private func setupHumidityParamView(with humidity: Int) {
        humidityParamView.setup(iconImage: Resources.Images.Common.humidity, valueString: "\(humidity)%")
    }
    
    private func setupVisibilityParamView(width visibility: Int) {
        visibilityParamView.setup(iconImage: Resources.Images.Common.eye, valueString: "\(visibility)km")
    }
}
