//
//  CurrentWeatherView.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.06.2022.
//

import UIKit

class CurrentWeatherView: UIView {
    
    //MARK: Properties
    private let padding: CGFloat = 10
    
    //MARK: - Views
    private lazy var nowLabel = UILabel(
        text: "Now",
        font: .systemFont(ofSize: 20, weight: .bold),
        color: .white,
        alignment: .center
    )
    
    private lazy var conditionLabel = UILabel(
        font: .systemFont(ofSize: 22, weight: .semibold),
        color: .white, lineLimit: 2,
        alignment: .center
    )
    
    private lazy var temperatureView = WeatherTemperatureView()
    
    private lazy var parametersListView = WeatherParametersListView()
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Resources.Colors.secondary
        addSubviews([nowLabel, conditionLabel, temperatureView, parametersListView])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    override func layoutSubviews() {
        let nowLabelHeight: CGFloat = 20
        let numberOfItems: CGFloat = 3
        
        let itemWidth = bounds.width - padding * 2
        let availableHeight = bounds.height - 2 * padding - nowLabelHeight
        let itemHeight = (availableHeight - padding * (numberOfItems + 1)) / numberOfItems
        
        nowLabel.frame = CGRect(
            x: padding,
            y: padding,
            width: itemWidth,
            height: nowLabelHeight
        )
        
        temperatureView.frame = CGRect(
            x: padding,
            y: nowLabel.frame.maxY + padding,
            width: itemWidth,
            height: itemHeight
        )
        
        conditionLabel.frame = CGRect(
            x: padding,
            y: temperatureView.frame.maxY + padding,
            width: itemWidth,
            height: itemHeight / 2
        )
        
        parametersListView.frame = CGRect(
            x: padding,
            y: conditionLabel.frame.maxY + padding,
            width: itemWidth,
            height: itemHeight + itemHeight / 2 + padding
        )
    }
    
    //MARK: - Methods
    func setup(temperature: Int,
               condition: String,
               weatherCode: Int,
               dayTime: DayTime,
               parameters: (windSpeed: Int, humidity: Int, visibility: Int)) {
        
        conditionLabel.text = condition
        
        temperatureView.setup(
            temperature: temperature,
            weatherCode: weatherCode,
            dayTime: dayTime
        )
        
        parametersListView.setup(
            windSpeed: parameters.windSpeed,
            humidity: parameters.humidity,
            visibility: parameters.visibility
        )
    }
}
