//
//  WeatherTemperatureView.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 01.07.2022.
//

import UIKit

class WeatherTemperatureView: UIView {
    
    //MARK: Properties
    private let padding: CGFloat = 5
    private let iconManager = WeatherIconManager()
    
    //MARK: - Views
    private lazy var weatherIcon = WeatherIconImageView()
    
    private lazy var temperatureLabel = UILabel(
        font: .systemFont(ofSize: 60, weight: .bold),
        color: .white
    )
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews([weatherIcon, temperatureLabel])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    override func layoutSubviews() {
        let availableWidth = bounds.width - padding * 2
        let availableHeight = bounds.height - padding * 2
        
        temperatureLabel.frame = CGRect(
            x: availableWidth / 2 + padding,
            y: padding,
            width: availableWidth / 2 - padding,
            height: availableHeight
        )
        
        
        weatherIcon.frame = CGRect(
            x: availableWidth / 2 - padding - availableHeight,
            y: padding,
            width: availableHeight,
            height: availableHeight
        )
        
    }
    
    //MARK: - Methods
    func setup(temperature: Int, weatherCode: Int, dayTime: DayTime) {
        temperatureLabel.text = temperature.toTemperature
        weatherIcon.image = iconManager.getIcon(code: weatherCode, dayTime: dayTime)
    }
    
}
