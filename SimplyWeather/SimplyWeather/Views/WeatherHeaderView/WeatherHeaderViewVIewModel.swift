//
//  WeatherHeaderViewViewModel.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 01.07.2022.
//

import Foundation

//MARK: - WeatherHeaderViewVIewModelProtocol
protocol WeatherHeaderViewViewModelProtocol {
    var city: String { get }
    var region: String { get }
    var date: String { get }
    var temperature: Int { get }
    var condition: String { get }
    var code: Int { get }
    var dayTime: DayTime { get }
    var windSpeed: Int { get }
    var humidity: Int { get }
    var visibility: Int { get }
}

//MARK: - WeatherHeaderViewVIewModel
class WeatherHeaderViewViewModel: WeatherHeaderViewViewModelProtocol {
    
    //MARK: Properties
    var city: String {
        location?.name ?? ""
    }
    
    var region: String {
        if let regionStr = location?.region, !regionStr.isEmpty {
            return regionStr
        } else {
            return location?.country ?? ""
        }
    }
    
    var date: String {
        Date.dateToString(date: Date(), format: "MMM d, yyyy")
    }
    
    var temperature: Int {
        (weather?.tempC ?? 0).roundedInteger
    }
    
    var condition: String {
        weather?.condition?.text ?? ""
    }
    
    var code: Int {
        weather?.condition?.code ?? 0
    }
    
    var dayTime: DayTime {
        DayTime(rawValue: weather?.isDay ?? 0) ?? .day
    }
    
    var windSpeed: Int {
        (weather?.windKph ?? 0).roundedInteger
    }
    
    var humidity: Int {
        weather?.humidity ?? 0
    }
    
    var visibility: Int {
        (weather?.visKM ?? 0).roundedInteger
    }
    
    private let weather: CurrentWeather?
    private let location: Location?
    
    //MARK: - Initialization
    init(weather: CurrentWeather?, location: Location?) {
        self.weather = weather
        self.location = location
    }
    
}
