//
//  DailyForecastTableViewCell.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 28.06.2022.
//

import UIKit

class DailyForecastTableViewCell: UITableViewCell {
    
    //MARK: Properties
    private var viewModel: DailyForecastCellViewModelProtocol! {
        didSet {
            updateUI()
        }
    }
    
    private let paddingWidth: CGFloat = 10
    private let paddingHeight: CGFloat = 5
    
    //MARK: - Views
    private lazy var weekdayLabel = UILabel(font: .systemFont(ofSize: 20, weight: .semibold), color: .white)
    
    private lazy var weatherIcon = WeatherIconImageView()
    
    private lazy var minAndMaxTempLabel = UILabel(font: .systemFont(ofSize: 20, weight: .semibold), color: .white, alignment: .center)
    
    //MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = Resources.Colors.secondary
        contentView.addSubviews([
            weekdayLabel,
            weatherIcon,
            minAndMaxTempLabel
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Methods
    func configure(with viewModel: DailyForecastCellViewModelProtocol) {
        self.viewModel = viewModel
    }
    
    //MARK: - Layout
    override func layoutSubviews() {
        calculateSizesOfSubviews()
    }
    
    private func calculateSizesOfSubviews() {
        let itemHeight = contentView.bounds.height - 2 * paddingHeight
        
        weekdayLabel.frame = CGRect(
            x: paddingWidth,
            y: paddingHeight,
            width: 60,
            height: itemHeight
        )
        
        weatherIcon.frame = CGRect(
            x: contentView.bounds.maxX - paddingWidth - 40,
            y: paddingHeight,
            width: 40,
            height: itemHeight
        )
        
        let availablbeWidth = contentView.bounds.width - (weekdayLabel.frame.width + weatherIcon.frame.width + paddingWidth * 4)
        
        minAndMaxTempLabel.frame = CGRect(
            x: weekdayLabel.frame.maxX + paddingWidth,
            y: paddingHeight,
            width: availablbeWidth,
            height: itemHeight
        )
    }
    
    //MARK: - Private methods
    private func updateUI() {
        weekdayLabel.text = viewModel.weekday
        minAndMaxTempLabel.text = "\(viewModel.minTemp)|\(viewModel.maxTemp.toTemperature)"
        weatherIcon.image = WeatherIconManager().getIcon(code: viewModel.code, dayTime: viewModel.dayTime)
    }
}
