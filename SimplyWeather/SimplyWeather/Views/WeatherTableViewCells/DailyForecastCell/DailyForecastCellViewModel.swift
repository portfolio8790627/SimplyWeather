//
//  DailyForecastCellViewModel.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.06.2022.
//

import Foundation

//MARK: - DailyForecastCellViewModelProtocol
protocol DailyForecastCellViewModelProtocol {
    var code: Int { get }
    var dayTime: DayTime { get }
    var minTemp: Int { get }
    var maxTemp: Int { get }
    var weekday: String { get }
}

//MARK: - DailyForecastCellViewModel
class DailyForecastCellViewModel: DailyForecastCellViewModelProtocol {
    
    //MARK: Properties
    var code: Int {
        day?.condition?.code ?? 0
    }
    
    var dayTime: DayTime {
        return .day
    }
    
    var minTemp: Int {
        (day?.mintempC ?? 0).roundedInteger
    }
    
    var maxTemp: Int {
        (day?.maxtempC ?? 0).roundedInteger
    }
    
    var weekday: String {
        getWeekday() ?? ""
    }
    
    private let day: Day?
    private let dateString: String
    
    //MARK: - Initialization
    init(day: Day?, dateString: String) {
        self.day = day
        self.dateString = dateString
    }
    
    //MARK: - Private methods
    private func getWeekday() -> String? {
        guard let date = Date.dateFromString(dateString, format: "yyyy-MM-dd") else {
            return nil
        }
        
        return Date.weekday(from: date)
    }
}
