//
//  HourlyForecastTableViewCellViewModel.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 28.06.2022.
//

import Foundation

//MARK: - HourlyTableViewCellViewModelProtocol
protocol HourlyTableViewCellViewModelProtocol {
    var numberOfHours: Int { get }
    func viewModelForCell(at indexPath: IndexPath) -> HourlyForecastCellViewModelProtocol
}

//MARK: - HourlyForecastTableViewCellViewModel
class HourlyForecastTableViewCellViewModel: HourlyTableViewCellViewModelProtocol {
    var numberOfHours: Int {
        forecastDay?.hour?.count ?? 0
    }
    
    private let forecastDay: ForecastDay?
    
    init(forecastDay: ForecastDay?) {
        self.forecastDay = forecastDay
    }
    
    //MARK: - Methods
    func viewModelForCell(at indexPath: IndexPath) -> HourlyForecastCellViewModelProtocol {
        return HourlyForecastCellViewModel(hour: forecastDay?.hour?[indexPath.item])
    }
    
    
}
