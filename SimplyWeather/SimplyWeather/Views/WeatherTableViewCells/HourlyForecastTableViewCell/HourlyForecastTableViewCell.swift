//
//  HourlyForecastTableViewCell.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 28.06.2022.
//

import UIKit

class HourlyForecastTableViewCell: UITableViewCell {
    
    //MARK: Properties
    private var viewModel: HourlyTableViewCellViewModelProtocol! {
        didSet {
            hourlyForecastCollectionView.reloadData()
        }
    }
    
    //MARK: - Views
    private lazy var hourlyForecastCollectionView: UICollectionView = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .horizontal
        
        let collectionView = UICollectionView(frame: CGRect(origin: .zero, size: .zero), collectionViewLayout: collectionViewLayout)
        collectionView.register(HourlyForecastCollectionViewCell.self, forCellWithReuseIdentifier: HourlyForecastCollectionViewCell.identifier)
        collectionView.backgroundColor = Resources.Colors.background
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    //MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCollectionView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    override func layoutSubviews() {
        hourlyForecastCollectionView.frame = bounds
    }
    
    //MARK: - Methods
    func configure(with viewModel: HourlyTableViewCellViewModelProtocol) {
        self.viewModel = viewModel
    }
    
    //MARK: - Private methods
    private func setupCollectionView() {
        contentView.addSubview(hourlyForecastCollectionView)
        hourlyForecastCollectionView.delegate = self
        hourlyForecastCollectionView.dataSource = self
    }
}

//MARK: - UICollectionViewDataSource
extension HourlyForecastTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfHours
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HourlyForecastCollectionViewCell.identifier, for: indexPath) as? HourlyForecastCollectionViewCell
        else {
            return UICollectionViewCell()
        }
        cell.viewModel = self.viewModel.viewModelForCell(at: indexPath)
        return cell
    }
}


//MARK: - UICollectionViewDelegate
extension HourlyForecastTableViewCell: UICollectionViewDelegate {}

//MARK: - UICollectionViewDelegateFlowLayout
extension HourlyForecastTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let availableHeight = collectionView.bounds.height
        return CGSize(width: availableHeight / 1.5, height: availableHeight)
    }
}
