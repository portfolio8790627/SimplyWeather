//
//  ForecastCellViewModel.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 26.06.2022.
//

import Foundation

//MARK: HourlyForecastCellViewModel
protocol HourlyForecastCellViewModelProtocol {
    var temperature: Int { get }
    var time: String { get }
    var code: Int { get }
    var dayTime: DayTime { get }
}

class HourlyForecastCellViewModel: HourlyForecastCellViewModelProtocol {
    var temperature: Int {
        (hour?.tempC ?? 0).roundedInteger
    }
    
    var time: String {
        createTime() ?? ""
    }
    
    var code: Int {
        hour?.condition?.code ?? 0
    }
    
    var dayTime: DayTime {
        DayTime(rawValue: hour?.isDay ?? 0) ?? .day
    }
    
    private let hour: Hour?
    
    //MARK: - Initialization
    init(hour: Hour?) {
        self.hour = hour
    }
    
    //MARK: - Private methods
    private func createTime() -> String? {
        guard let timeString = hour?.time,
              let date = Date.dateFromString(timeString, format: "yyyy-MM-dd HH:mm")
        else { return nil }
        return Date.dateToString(date: date, format: "ha")
    }
}
