//
//  HourlyForecastCollectionViewCell.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 26.06.2022.
//

import UIKit

class HourlyForecastCollectionViewCell: UICollectionViewCell {
    
    //MARK: Properties
    static let identifier = "ForecastCell"
    private let padding: CGFloat = 5
    private let iconManager = WeatherIconManager()
    
    var viewModel: HourlyForecastCellViewModelProtocol! {
        didSet {
            timeLabel.text = viewModel.time
            temperatureLabel.text = viewModel.temperature.toTemperature
            weatherIcon.image = iconManager.getIcon(
                code: viewModel.code,
                dayTime: viewModel.dayTime
            )
        }
    }
    
    //MARK: - Views
    private lazy var timeLabel = UILabel(color: .white, alignment: .center)
    
    private lazy var weatherIcon = WeatherIconImageView()
    
    private lazy var temperatureLabel = UILabel(
        font: .systemFont(ofSize: 20, weight: .semibold),
        color: .white,
        alignment: .center
    )

    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = Resources.Colors.secondary
        contentView.layer.cornerRadius = contentView.frame.height / 10
        contentView.addSubviews([
            timeLabel,
            weatherIcon,
            temperatureLabel
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        calculateSubviewsSizes()
    }
    
    //MARK: - Private methods
    private func calculateSubviewsSizes() {
        let numberOfItems: CGFloat = 3
        
        let itemWidth = contentView.bounds.width - padding * 2
        let itemHeight = (contentView.bounds.height - padding * (numberOfItems + 1)) / numberOfItems
        
        timeLabel.frame = CGRect(x: padding, y: padding, width: itemWidth, height: itemHeight)
        
        weatherIcon.frame = CGRect(
            x: padding,
            y: timeLabel.frame.maxY + padding,
            width: itemWidth,
            height: itemHeight
        )
        
        temperatureLabel.frame = CGRect(
            x: padding,
            y: weatherIcon.frame.maxY + padding,
            width: itemWidth,
            height: itemHeight
        )
    }
}
