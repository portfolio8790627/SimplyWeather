//
//  WeatherIconImageView.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.06.2022.
//

import UIKit

class WeatherIconImageView: UIImageView {
    
    //MARK: Initialization
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private methods
    private func setup() {
        contentMode = .scaleAspectFit
        tintColor = .white
        clipsToBounds = true
    }
}
