//
//  AssemblyBuilder.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import UIKit

//MARK: - AssemblyBuilderErrors
fileprivate enum AssemblyBuilderErrors {
    case invalidCoordinator
    case unavailableAPIKey
    
    var description: String {
        var errorDescription = "AssemblyBuilder error: "
        
        switch self {
        case .invalidCoordinator:
            errorDescription += "Invalid coordinator has been defined for the module"
        case .unavailableAPIKey:
            errorDescription += "The API key cannot be extracted"
        }
        
        return errorDescription
    }
}

//MARK: AssemblyBuilderProtocol

protocol AssemblyBuilderProtocol {
    func createWeatherModule(coordinator: Coordinator) -> UIViewController
}

//MARK: - AssemblyBuilder

class AssemblyBuilder: AssemblyBuilderProtocol {
    
    //MARK: - Methods
    
    func createWeatherModule(coordinator: Coordinator) -> UIViewController {
        guard let weatherCoordinator = coordinator as? WeatherCoordinator else {
            fatalError(AssemblyBuilderErrors.invalidCoordinator.description)
        }
        
        let view = WeatherViewController()
        
        let apiManager = WeatherAPIManager(apiKey: prepareAPIKey(), networkService: NetworkService())
        let weatherDataExtractor = WeatherAPIDataExtractor(apiManager: apiManager)
        
        let weatherViewModel = WeatherViewModel(
            weatherDataExtractor: weatherDataExtractor,
            coordinator: weatherCoordinator,
            netConnectionManager: NetworkConnectionManager()
        )
        view.configure(with: weatherViewModel)
        
        return view
    }
    
    //MARK: - Private methods
    
    private func prepareAPIKey() -> String {
        let manager = APIKeyManager()
        guard let apiKey = manager.getAPIKey() else {
            fatalError(AssemblyBuilderErrors.unavailableAPIKey.description)
        }
        return apiKey
    }
    
}
