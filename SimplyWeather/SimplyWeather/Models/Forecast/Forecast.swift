//
//  Forecast.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 26.06.2022.
//

import Foundation

// MARK: - Forecast
struct Forecast: Codable {
    let forecastday: [ForecastDay]?
}

// MARK: - Forecastday
struct ForecastDay: Codable {
    let date: String?
    let dateEpoch: Int?
    let day: Day?
    let astro: Astro?
    let hour: [Hour]?

    enum CodingKeys: String, CodingKey {
        case date
        case dateEpoch = "date_epoch"
        case day, astro, hour
    }
}
