//
//  Weather.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 26.06.2022.
//

import Foundation

// MARK: - Weather
struct Weather: Codable {
    let location: Location?
    let current: CurrentWeather?
    let forecast: Forecast?
}

// MARK: - Condition
struct Condition: Codable {
    let text: String?
    let icon: String?
    let code: Int?
}

