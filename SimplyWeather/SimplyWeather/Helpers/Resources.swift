//
//  Resources.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.07.2022.
//

import UIKit

enum Resources {
    enum Colors {
        static let background = UIColor("#16063e")
        static let secondary = UIColor("#2e1b5e")
    }
    
    enum Images {
        enum Weather {
            static let sun = UIImage(systemName: "sun.max.fill")
            static let moon = UIImage(systemName: "moon.fill")
            
            static let cloudSun = UIImage(systemName: "cloud.sun.fill")
            static let cloudMoon = UIImage(systemName: "cloud.moon.fill")
            
            static let cloud = UIImage(systemName: "cloud.fill")
            
            static let fog = UIImage(systemName: "cloud.fog.fill")
            
            static let cloudSunRain = UIImage(systemName: "cloud.sun.rain.fill")
            static let cloudMoonRain = UIImage(systemName: "cloud.moon.rain.fill")
            
            static let cloudSnow = UIImage(systemName: "cloud.snow.fill")
            
            static let cloudSleet = UIImage(systemName: "cloud.sleet.fill")
            
            static let cloudDrizzle = UIImage(systemName: "cloud.drizzle.fill")
            
            static let cloudSunBolt = UIImage(systemName: "cloud.sun.bolt.fill")
            static let cloudMoonBolt = UIImage(systemName: "cloud.moon.bolt.fill")
            
            static let windSnow = UIImage(systemName: "wind.snow")
            
            static let cloudHeavyRain = UIImage(systemName: "cloud.heavyrain.fill")
            
            static let snowflake = UIImage(systemName: "snowflake")
            
            static let cloudHail = UIImage(systemName: "cloud.hail.fill")
            
            static let cloudBoldRain = UIImage(systemName: "cloud.bolt.rain.fill")
        }
        
        enum Common {
            static let noWifi = UIImage(systemName: "wifi.slash")
            
            static let wind = UIImage(systemName: "wind")
            static let humidity = UIImage(systemName: "humidity.fill")
            static let eye = UIImage(systemName: "eye")
        }
    }
    
    enum Strings {
        enum Buttons {
            static let retry = "Retry"
        }
        
        enum Weather {
            static let hourlyForecast = "Hourly Forecast"
            static let threeDayForecast = "3-Day Forecast"
        }
        
        static let noInternetConnection = "You are disconnected from the Internet"
    }
}
