//
//  Extension + Date.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 27.06.2022.
//

import Foundation

extension Date {
    static func dateFromString(_ str: String, format: String) -> Date? {
        let formatter = DateFormatter.createFormatter(with: format)
        return formatter.date(from: str)
    }
    
    static func dateToString(date: Date, format: String) -> String {
        let formatter = DateFormatter.createFormatter(with: format)
        return formatter.string(from: date)
    }
    
    static func weekday(from date: Date) -> String {
        let formatter = DateFormatter.createFormatter(with: "E")
        return formatter.string(from: date)
    }
}
