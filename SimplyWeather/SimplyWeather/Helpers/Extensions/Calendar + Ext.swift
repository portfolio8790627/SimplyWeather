//
//  Extension + Calendar.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 03.07.2022.
//

import Foundation

extension Calendar {
    func minutes(from startDate: Date, to endDate: Date) -> Int? {
        return self.dateComponents([.minute], from: startDate, to: endDate).minute
    }
}
