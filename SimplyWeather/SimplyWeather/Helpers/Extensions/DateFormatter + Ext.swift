//
//  Extension + DateFormatter.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.06.2022.
//

import Foundation

extension DateFormatter {
    static func createFormatter(with dateFormat: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = dateFormat
        return formatter
    }
}
