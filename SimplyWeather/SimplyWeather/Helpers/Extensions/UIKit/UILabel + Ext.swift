//
//  Extension + UILabel.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.06.2022.
//

import UIKit

extension UILabel {
    convenience init(text: String? = nil,
                     font: UIFont = .systemFont(ofSize: 17),
                     color: UIColor = .label,
                     lineLimit: Int = 1,
                     alignment: NSTextAlignment = .natural) {
        self.init()
        
        self.text = text
        self.font = font
        self.textColor = color
        self.numberOfLines = lineLimit
        self.textAlignment = alignment
    }
}
