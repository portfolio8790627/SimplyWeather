//
//  Extension + UIView.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 01.07.2022.
//

import UIKit

extension UIView {
    func addSubviews(_ views: [UIView]) {
        views.forEach { addSubview($0) }
    }
}
