//
//  Extension + Double.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 02.07.2022.
//

import Foundation

extension Double {
    var roundedInteger: Int {
        Int(self.rounded())
    }
}
