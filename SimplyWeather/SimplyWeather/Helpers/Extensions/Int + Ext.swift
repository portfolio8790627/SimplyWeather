//
//  Int + Ext.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 30.07.2022.
//

import Foundation

extension Int {
    var toTemperature: String {
        return "\(self)º"
    }
}
