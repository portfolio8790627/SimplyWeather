//
//  WeatherDataExtractorProtocol.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 25.06.2022.
//

import CoreLocation

protocol WeatherDataExtractorProtocol {
    /// Extracts weather forecast data from a JSON model
    /// - Parameters:
    ///   - location: The location for which the weather forecast is requested
    ///   - numberOfDays: The number of days for which you need to get a forecast
    ///   - completion: Completion handler
    func getWeatherForecast(for location: CLLocation, numberOfDays: Int, completion: @escaping (Weather?) -> Void)
}
