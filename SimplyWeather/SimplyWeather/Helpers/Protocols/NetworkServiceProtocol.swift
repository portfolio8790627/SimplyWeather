//
//  NetworkServiceProtocol.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import Foundation

protocol NetworkServiceProtocol {

    /// Creates an URL from the corresponding parameters
    /// - Parameters:
    ///   - scheme: The scheme subcomponent of the URL
    ///   - host: The host subcomponent
    ///   - queryParameters: An array of query items for the URL
    /// - Returns: A link created based on URL parameters
    func createURL(scheme: String, host: String, path: String, queryParameters: [String: String]?) -> URL?
    
    /// Creates a URL Session task
    /// - Parameters:
    ///   - url: Link to download data
    ///   - completion: Completion handler
    /// - Returns: The URL session task with the downloaded data
    func createDataTask(with url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}
