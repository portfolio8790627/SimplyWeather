//
//  NetworkConnectionManagerProtocol.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 01.07.2022.
//

import Foundation

protocol NetworkConnectionManagerProtocol {
    /// Property for tracking changes in the Internet connection
    var connectionDidChange: ((Bool) -> Void)? { get set }
}
