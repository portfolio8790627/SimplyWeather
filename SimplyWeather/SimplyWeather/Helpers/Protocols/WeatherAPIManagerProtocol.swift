//
//  WeatherAPIManagerProtocol.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import CoreLocation

protocol WeatherAPIMangerProtocol {
    /// Makes a request to the server to get the weather forecast by coordinates
    /// - Parameters:
    ///   - latitude: Latitude coordinates
    ///   - longitude: Longitude coordinates
    ///   - completion: completion handler
    func getForecast(latitude: CLLocationDegrees, longitude: CLLocationDegrees, numberOfDays: Int, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

