//
//  WeatherCoordinator.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 25.06.2022.
//

import UIKit

class WeatherCoordinator: Coordinator {
    
    //MARK: Properties
    var navigationController: UINavigationController
    
    var assemblyBuilder: AssemblyBuilderProtocol
    
    weak var parentCoordinator: Coordinator?
    
    var childCoordinators: [Coordinator] = []
    
    //MARK: - Initialization
    init(navighationController: UINavigationController, assemblyBuilder: AssemblyBuilderProtocol) {
        self.navigationController = navighationController
        self.assemblyBuilder = assemblyBuilder
    }
    
    //MARK: - Methods
    func start() {
        let viewController = assemblyBuilder.createWeatherModule(coordinator: self)
        navigationController.pushViewController(viewController, animated: false)
    }
    
    func showBadConnectionController() {
        let coordinator = BadConnectionCoordinator(navigationController: navigationController, assemblyBuilder: assemblyBuilder)
        coordinator.parentCoordinator = self
        childCoordinators.append(coordinator)
        coordinator.start()
    }
    
    func childDidFinish(_ child: Coordinator) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                if coordinator is BadConnectionCoordinator {
                    let vc = assemblyBuilder.createWeatherModule(coordinator: self)
                    navigationController.viewControllers = [vc]
                }
                break
            }
        }
    }
}
