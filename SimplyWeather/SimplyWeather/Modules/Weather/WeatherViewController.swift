//
//  WeatherViewController.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import UIKit

class WeatherViewController: UIViewController {
    
    //MARK: Properties
    private var viewModel: WeatherViewModelProtocol! {
        didSet {
            viewModel.viewModelDidChange = { [weak self] changedViewModel in
                guard let sSelf = self else { return }
                sSelf.weatherHeaderView.configure(with: changedViewModel.viewModelForWeatherHeader())
                sSelf.displayWeatherData()
            }
            
            viewModel.updateLocation()
        }
    }
    
    //MARK: - Views
    private lazy var weatherTableView = UITableView(frame: .zero, style: .insetGrouped)
    
    private lazy var weatherHeaderView = WeatherHeaderView(
        frame: CGRect(
            origin: .zero,
            size: CGSize(
                width: view.bounds.width - 40,
                height: 400
            )
        )
    )
    
    private lazy var loadingIndicatorView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.color = .white
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    //MARK: - View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Weather"
        navigationController?.navigationBar.prefersLargeTitles = true
        view.backgroundColor = Resources.Colors.background
        setupLoadingIndicator()
        setupTableView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateWeatherDataAfterBackground), name: Notification.Name("UpdateAfterBackground"), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        loadingIndicatorView.center = view.center
        weatherTableView.frame = view.bounds
        view.sendSubviewToBack(weatherTableView)
    }
    
    //MARK: - Methods
    func configure(with viewModel: WeatherViewModelProtocol) {
        self.viewModel = viewModel
    }
    
    //MARK: - Ations
    @objc private func updateWeatherDataAfterBackground() {
        reloadWeatherData()
    }
}

//MARK: - Private methods
private extension WeatherViewController {
    func setupTableView() {
        view.addSubview(weatherTableView)
        weatherTableView.backgroundColor = Resources.Colors.background
        weatherTableView.isHidden = true
        weatherTableView.showsVerticalScrollIndicator = false
        weatherTableView.showsHorizontalScrollIndicator = false
        weatherTableView.tableHeaderView = weatherHeaderView
        weatherTableView.dataSource = self
        weatherTableView.delegate = self
    }
    
    func setupLoadingIndicator() {
        view.addSubview(loadingIndicatorView)
        loadingIndicatorView.startAnimating()
    }
    
    func displayWeatherData() {
        loadingIndicatorView.stopAnimating()
        weatherTableView.isHidden = false
        weatherTableView.reloadData()
    }
    
    func reloadWeatherData() {
        weatherTableView.isHidden = true
        loadingIndicatorView.startAnimating()
        viewModel.updateLocation()
    }
}

//MARK: - UITableViewDataSource
extension WeatherViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfItems(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = viewModel.tableViewCellType(for: indexPath.section)
        
        switch cellType {
        case .hourlyForecast:
            let cell = HourlyForecastTableViewCell()
            cell.configure(with: viewModel.viewModelForHourlyForecastCell())
            return cell
        case .dailyForecast:
            let cell = DailyForecastTableViewCell()
            cell.configure(with: viewModel.viewModelForDailyForecastCell(at: indexPath))
            return cell
        }
    }
}

//MARK: - UITableViewDelegate
extension WeatherViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType = viewModel.tableViewCellType(for: indexPath.section)
        switch cellType {
        case .hourlyForecast:
            return 110
        case .dailyForecast:
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeader(in: section)
    }
}

