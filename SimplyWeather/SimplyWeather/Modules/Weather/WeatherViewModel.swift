//
//  WeatherViewModel.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import CoreLocation

//MARK: WeatherSections
enum WeatherSections: Int, CaseIterable {
    case hourlyForecast
    case dailyForecast
}

//MARK: - WeatherViewModelProtocol
protocol WeatherViewModelProtocol {
    var numberOfSections: Int { get }
    var viewModelDidChange: ((WeatherViewModelProtocol) -> Void)? { get set }
    func updateLocation()
    func viewModelForHourlyForecastCell() -> HourlyTableViewCellViewModelProtocol
    func viewModelForDailyForecastCell(at indexPath: IndexPath) -> DailyForecastCellViewModelProtocol
    func numberOfItems(in section: Int) -> Int
    func tableViewCellType(for section: Int) -> WeatherSections
    func titleForHeader(in section: Int) -> String
    func viewModelForWeatherHeader() -> WeatherHeaderViewViewModelProtocol
}

//MARK: - WeatherViewModel
class WeatherViewModel: WeatherViewModelProtocol {
    
    //MARK: Properties
    var numberOfSections: Int {
        WeatherSections.allCases.count
    }

    var viewModelDidChange: ((WeatherViewModelProtocol) -> Void)?
        
    private var weather: Weather? {
        didSet {
            viewModelDidChange?(self)
        }
    }
    
    private var weatherDataExtractor: WeatherDataExtractorProtocol
    private let coordinator: WeatherCoordinator
    private var netConnectionManager: NetworkConnectionManagerProtocol
    private let locationManager = UserLocationManager()
    
    //MARK: - Initialization
    init(weatherDataExtractor: WeatherDataExtractorProtocol, coordinator: WeatherCoordinator, netConnectionManager: NetworkConnectionManagerProtocol) {
        self.weatherDataExtractor = weatherDataExtractor
        self.coordinator = coordinator
        self.netConnectionManager = netConnectionManager
        self.netConnectionManager.connectionDidChange = { [unowned self] isConnected in
            if !isConnected {
                reportBadConnection()
            }
        }
        
        locationManager.delegate = self
    }
    
    //MARK: - Methods
    func updateLocation() {
        locationManager.updateLocation()
    }
    
    func viewModelForHourlyForecastCell() -> HourlyTableViewCellViewModelProtocol {
        HourlyForecastTableViewCellViewModel(forecastDay: weather?.forecast?.forecastday?.first)
    }
    
    func viewModelForDailyForecastCell(at indexPath: IndexPath) -> DailyForecastCellViewModelProtocol {
        DailyForecastCellViewModel(
            day: weather?.forecast?.forecastday?[indexPath.row].day,
            dateString: weather?.forecast?.forecastday?[indexPath.row].date ?? ""
        )
    }
    
    func numberOfItems(in section: Int) -> Int {
        let weatherSection = createWeatherSection(value: section)
        switch weatherSection {
        case .hourlyForecast:
            return 1
        case .dailyForecast:
            return weather?.forecast?.forecastday?.count ?? 0
        }
    }
    
    func tableViewCellType(for section: Int) -> WeatherSections {
        return createWeatherSection(value: section)
    }
    
    func titleForHeader(in section: Int) -> String {
        let weatherSection = createWeatherSection(value: section)
        switch weatherSection {
        case .hourlyForecast:
            return Resources.Strings.Weather.hourlyForecast
        case .dailyForecast:
            return Resources.Strings.Weather.threeDayForecast
        }
    }
    
    func viewModelForWeatherHeader() -> WeatherHeaderViewViewModelProtocol {
        WeatherHeaderViewViewModel(weather: weather?.current, location: weather?.location)
    }
}

//MARK: - Private methods
extension WeatherViewModel {
    private func getForecast(for location: CLLocation?) {
        guard let location = location else { return }
        weatherDataExtractor.getWeatherForecast(for: location, numberOfDays: 10) { [weak self] weather in
            self?.weather = weather
        }
    }
    
    private func createWeatherSection(value: Int) -> WeatherSections {
        guard let weatherSection = WeatherSections(rawValue: value) else {
            fatalError("Error: Discrepancy between the number of sections and the types of sections")
        }
        return weatherSection
    }
    
    private func reportBadConnection() {
        coordinator.showBadConnectionController()
    }
}

//MARK: - UserLocationManagerDelegate

extension WeatherViewModel: UserLocationManagerDelegate {
    func locationUpdated(with location: CLLocation?) {
        getForecast(for: location)
    }
    
    func locationUpdateFailed(with error: Error) {
        print(error.localizedDescription)
    }
}

