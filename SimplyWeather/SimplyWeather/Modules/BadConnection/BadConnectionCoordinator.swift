//
//  BadConnectionCoordinator.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 02.07.2022.
//

import UIKit

class BadConnectionCoordinator: Coordinator {
    
    //MARK: Properties
    var navigationController: UINavigationController
    
    var assemblyBuilder: AssemblyBuilderProtocol
    
    weak var parentCoordinator: Coordinator?
    
    var childCoordinators: [Coordinator] = []
    
    //MARK: - Initialization
    init(navigationController: UINavigationController, assemblyBuilder: AssemblyBuilderProtocol) {
        self.navigationController = navigationController
        self.assemblyBuilder = assemblyBuilder
    }
    
    //MARK: - Methods
    func start() {
        let viewController = BadConnectionViewController()
        viewController.coordinator = self
        viewController.modalPresentationStyle = .fullScreen
        navigationController.present(viewController, animated: true)
    }
    
    func coordinatorDidFinishWork() {
        parentCoordinator?.childDidFinish(self)
    }
    
    func childDidFinish(_ child: Coordinator) {}
}
