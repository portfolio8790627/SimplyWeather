//
//  BadConnectionViewController.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 02.07.2022.
//

import UIKit

class BadConnectionViewController: UIViewController {
    
    //MARK: Properties
    weak var coordinator: BadConnectionCoordinator?
    private let padding: CGFloat = 10
    
    //MARK: - Views
    private lazy var badConnectionLabel = UILabel(
        text: Resources.Strings.noInternetConnection.uppercased(),
        font: .systemFont(ofSize: 25, weight: .bold),
        color: .white,
        lineLimit: 0,
        alignment: .center
    )
    
    private lazy var retryButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(Resources.Strings.Buttons.retry, for: .normal)
        button.backgroundColor = .white
        button.tintColor = Resources.Colors.background
        button.titleLabel?.font = .systemFont(ofSize: 20, weight: .semibold)
        button.addTarget(self, action: #selector(retryButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var noInternetImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Images.Common.noWifi)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        return imageView
    }()

    //MARK: - View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Resources.Colors.background
        view.addSubviews([
            badConnectionLabel,
            noInternetImageView,
            retryButton
        ])
        setConstraints()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        coordinator?.coordinatorDidFinishWork()
    }
    
    override func viewDidLayoutSubviews() {
        retryButton.layer.cornerRadius = retryButton.frame.height / 2
    }
    
    //MARK: - Ations
    @objc private func retryButtonTapped() {
        self.dismiss(animated: true)
    }

    //MARK: - Private methods
    private func setConstraints() {
        [badConnectionLabel, noInternetImageView, retryButton]
            .forEach { view in
                view.translatesAutoresizingMaskIntoConstraints = false
            }
        
        NSLayoutConstraint.activate([
            noInternetImageView.heightAnchor.constraint(equalToConstant: 150),
            noInternetImageView.widthAnchor.constraint(equalToConstant: 150),
            noInternetImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            noInternetImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -padding)
        ])
        
        NSLayoutConstraint.activate([
            badConnectionLabel.bottomAnchor.constraint(equalTo: noInternetImageView.topAnchor, constant: -padding),
            badConnectionLabel.leadingAnchor.constraint(equalTo: safeLeadingAnchor, constant: 16),
            badConnectionLabel.trailingAnchor.constraint(equalTo: safeTrailingAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            retryButton.topAnchor.constraint(equalTo: noInternetImageView.bottomAnchor, constant: padding),
            retryButton.widthAnchor.constraint(equalToConstant: 100),
            retryButton.heightAnchor.constraint(equalToConstant: 50),
            retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}
