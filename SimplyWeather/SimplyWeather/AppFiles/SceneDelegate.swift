//
//  SceneDelegate.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    private let backgroundTimeKey = "timeInBackground"
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let navController = UINavigationController()
        
        let coordinator = WeatherCoordinator(
            navighationController: navController,
            assemblyBuilder: AssemblyBuilder()
        )
        coordinator.start()
        
        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = coordinator.navigationController
        window?.makeKeyAndVisible()
        window?.overrideUserInterfaceStyle = .dark
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        updateDataAfterBackground()
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        UserDefaults.standard.set(Date(), forKey: backgroundTimeKey)
    }
    
    //MARK: - Private methods
    private func updateDataAfterBackground() {
        guard let date = UserDefaults.standard.object(forKey: backgroundTimeKey) as? Date,
              let minutes = Calendar.current.minutes(from: date, to: Date()),
              minutes >= 5
        else { return }
        
        NotificationCenter.default.post(name: Notification.Name("UpdateAfterBackground"), object: nil)
    }
}


