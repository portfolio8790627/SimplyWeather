//
//  APIKeyManager.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import Foundation

struct APIKeyManager {
    func getAPIKey() -> String? {
        guard let path = Bundle.main.path(forResource: "Info", ofType: ".plist") else { return nil }
        do {
            let dictionary = try NSDictionary(contentsOf: URL(fileURLWithPath: path), error: ())
            guard let key = dictionary.object(forKey: "APIKey") as? String else { return nil }
            return key
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
}
