//
//  UserLocationManager.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import CoreLocation

//MARK: - UserLocationManagerDelegate
protocol UserLocationManagerDelegate: AnyObject {
    func locationUpdated(with location: CLLocation?)
    func locationUpdateFailed(with error: Error)
}

//MARK: - UserLocationManager
class UserLocationManager: NSObject {
    
    //MARK: Properties
    weak var delegate: UserLocationManagerDelegate?
    
    private let manager = CLLocationManager()
    private var currentLocation: CLLocation? {
        didSet {
            delegate?.locationUpdated(with: currentLocation)
        }
    }
    
    //MARK: - Initialization
    override init() {
        super.init()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
    }
    
    //MARK: - Private methods
    func updateLocation() {
        manager.startUpdatingLocation()
    }
}

//MARK: - CLLocationManagerDelegate
extension UserLocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard
            !locations.isEmpty,
            let location = locations.first
        else { return }
        currentLocation = location
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.locationUpdateFailed(with: error)
    }
}
