//
//  JSONHelper.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import Foundation

class JSONHelper {
    func decodeJSON<T: Decodable>(type: T.Type, from data: Data) -> T? {
        do {
            let item = try JSONDecoder().decode(type.self, from: data)
            return item
        } catch let decodingError {
            print(decodingError.localizedDescription)
            return nil
        }
    }
}
