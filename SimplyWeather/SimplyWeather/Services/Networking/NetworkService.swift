//
//  NetworkService.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import Foundation

struct NetworkService: NetworkServiceProtocol {
    func createURL(scheme: String, host: String, path: String, queryParameters: [String : String]?) -> URL? {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = path
        if let queryParameters = queryParameters {
            components.queryItems = queryParameters.map { key, value in
                URLQueryItem(name: key, value: value)
            }
        }
        return components.url
    }
    
    func createDataTask(with url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                completion(data, response, error)
            }
        }
        return dataTask
    }
}
