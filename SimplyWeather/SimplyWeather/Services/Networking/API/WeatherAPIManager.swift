//
//  WeatherAPIManager.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import CoreLocation

//MARK: - APIBase
fileprivate enum APIBase: String {
    case scheme = "https"
    case host = "api.weatherapi.com"
}

//MARK: - APIRequestType
fileprivate enum APIRequestType {
    case forecast
    
    var path: String {
        switch self {
        case .forecast:
            return "/v1/forecast.json"
        }
    }
}

//MARK: - WeatherAPIManager
class WeatherAPIManager: WeatherAPIMangerProtocol {
    
    //MARK: Properties
    private let networkService: NetworkServiceProtocol
    private let apiKey: String
    
    //MARK: - Initialization
    init(apiKey: String, networkService: NetworkServiceProtocol) {
        self.apiKey = apiKey
        self.networkService = networkService
    }
    
    //MARK: - Methods
    func getForecast(latitude: CLLocationDegrees, longitude: CLLocationDegrees, numberOfDays: Int, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        guard let url = networkService.createURL(
            scheme: APIBase.scheme.rawValue,
            host: APIBase.host.rawValue,
            path: APIRequestType.forecast.path,
            queryParameters: prepareQueryParameters(
                latitude: latitude,
                longitide: longitude,
                numberOfDays: numberOfDays
            )
        ) else { return }
        
        let task = networkService.createDataTask(with: url, completion: completion)
        task.resume()
    }
    
    //MARK: - Private methods
    private func createTask(with url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = networkService.createDataTask(with: url, completion: completion)
        task.resume()
    }
    
    private func prepareQueryParameters(latitude: CLLocationDegrees, longitide: CLLocationDegrees, numberOfDays: Int) -> [String: String] {
        var parameters: [String: String] = [:]
        parameters["q"] = "\(latitude), \(longitide)"
        parameters["key"] = apiKey
        parameters["days"] = "\(numberOfDays)"
        return parameters
    }

    
}
