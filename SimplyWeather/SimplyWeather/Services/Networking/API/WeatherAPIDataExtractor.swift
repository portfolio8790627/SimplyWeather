//
//  WeatherAPIDataExtractor.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 23.06.2022.
//

import CoreLocation

class WeatherAPIDataExtractor: WeatherDataExtractorProtocol {
    
    //MARK: Properties
    private let apiManager: WeatherAPIMangerProtocol
    private let helper = JSONHelper()
    
    //MARK: - Initialization
    init(apiManager: WeatherAPIMangerProtocol) {
        self.apiManager = apiManager
    }
    
    //MARK: - Methods
    func getWeatherForecast(for location: CLLocation, numberOfDays: Int, completion: @escaping (Weather?) -> Void) {
        let coordinate = location.coordinate
        apiManager.getForecast(latitude: coordinate.latitude, longitude: coordinate.longitude, numberOfDays: numberOfDays) { [unowned self] data, response, error in
            guard isSutiable(data: data, response: response, error: error) else { return }
            
            let weatherForecast = helper.decodeJSON(type: Weather.self, from: data!)
            completion(weatherForecast)
        }
    }
    
    //MARK: - Private methods
    private func isSutiable(data: Data?, response: URLResponse?, error: Error?) -> Bool {
        if let error = error {
            print(error.localizedDescription)
            return false
        }
        
        guard let httpResponse = response as? HTTPURLResponse,
              httpResponse.statusCode == 200,
              data != nil
        else {
            return false
        }
        
        return true
    }
}
