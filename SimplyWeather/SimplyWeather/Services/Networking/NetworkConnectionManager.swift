//
//  NetworkConnectionManager.swift
//  SimplyWeather
//
//  Created by Малиль Дугулюбгов on 01.07.2022.
//

import Network

class NetworkConnectionManager: NetworkConnectionManagerProtocol {
    
    //MARK: Properties
    
    var connectionDidChange: ((_ isConnected: Bool) -> Void)?
    
    private let queue = DispatchQueue(label: "NetworkConnection")
    private let monitor = NWPathMonitor()
    
    //MARK: - Initialization
    init() {
        monitor.pathUpdateHandler = { [weak self] path in
            DispatchQueue.main.async {
                let isConnected = path.status == .satisfied
                self?.connectionDidChange?(isConnected)
            }
        }
        
        monitor.start(queue: queue)
    }
}
