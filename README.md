# SimplyWeather
Swift IOS Application

![180](https://user-images.githubusercontent.com/81229461/177046577-7d481758-68bf-46a1-a2eb-11cb56245acb.png)

SimplyWeather is an application for viewing weather information based on your location. 

You can get several types of weather information:
  * Weather for the current location
  * Hourly forecast for today
  * Three-day weather forecast

![image](https://user-images.githubusercontent.com/81229461/177046558-9a042c12-4c7e-4752-8306-c8efec31bccd.png)

Additional functionality is available in the application:
  * Automatic data update after starting from background mode
  * Checking Internet connection
